module Widgets.TodoApp where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM (section)
import Concur.React.Props (className)

import Widgets.Header (headerWidget)
import Widgets.Main (mainWidget)
import Widgets.Footer (Filter(..), footerWidget)

todoAppWidget :: Widget HTML Unit
todoAppWidget = 
    section 
        [ className "todoapp" ] 
        [ headerWidget
        , mainWidget
        , footerWidget All
        ]


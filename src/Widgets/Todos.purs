module Widgets.TodoList where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM (ul)
import Concur.React.Props (className)
import Widgets.Todo (Todo, todoWidget)

todoListWidget :: Array Todo -> Widget HTML Unit
todoListWidget ts = 
    ul
        [ className "todo-list" ]
        (map todoWidget ts)


module Widgets.Todo where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM (button, div, input, label', li, text)
import Concur.React.Props (_type, checked, className, value)

type Todo = 
    { userId :: Int
    , id :: Int
    , title :: String
    , completed :: Boolean 
    }

todoWidget :: Todo -> Widget HTML Unit
todoWidget todo = 
    li 
        [ className (if todo.completed then "completed" else "") ]
        [ div 
            [ className "view" ] 
            [ input
                [ className "toggle" 
                , _type "checkbox"
                , checked true
                ]
            , label'
                [ text todo.title ]
            , button
                [ className "destroy" ]
                []
            ]
        , input
            [ className "edit" 
            , value todo.title 
            ]
        ]


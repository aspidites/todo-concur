module Widgets.NewTodo where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM (input)
import Concur.React.Props (autoFocus, className, placeholder)

newTodoWidget :: Widget HTML Unit
newTodoWidget =
    input 
        [ className "new-todo" 
        , placeholder "What needs to be done?"
        , autoFocus true
        ]

module Widgets.Header where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM (h1', header, text)
import Concur.React.Props (className)
import Widgets.NewTodo (newTodoWidget)

headerWidget :: Widget HTML Unit
headerWidget =
    header 
        [ className "header" ] 
        [ h1'
            [text "todos" ]
        , newTodoWidget
        ]


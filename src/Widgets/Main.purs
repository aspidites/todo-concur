module Widgets.Main where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM (input, label, section, text)
import Concur.React.Props (_id, _type, className, htmlFor)
import Widgets.TodoList (todoListWidget)
import Widgets.Todo (Todo)

todos :: Array Todo
todos =
    [ { userId: 1
      , id: 1
      , title: "delectus aut autem"
      , completed: false 
      }
    ]

mainWidget :: Widget HTML Unit
mainWidget = 
    section
        [ className "main" ]
        [ input 
            [ _id "toggle-all"
            , className "toggle-all"
            , _type "checkbox" 
            ]
        , label
            [ htmlFor "toggle-all" ]
            [ text "Mark all as complete" ]
        , todoListWidget todos
        ]

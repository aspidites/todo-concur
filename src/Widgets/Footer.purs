module Widgets.Footer where

import Prelude hiding (div)
import Concur.Core (Widget)
import Concur.React (HTML)
import Concur.React.DOM 
    ( a
    , button
    , footer
    , li'
    , span
    , strong'
    , text
    , ul
    )
import Concur.React.Props (className, href)

data Filter
    = All
    | Active
    | Completed

instance eqFilter :: Eq Filter where
    eq All All = true
    eq Active Active = true
    eq Completed Completed = true
    eq _ _ = false

footerWidget :: Filter -> Widget HTML Unit
footerWidget filter =
    footer
        [ className "footer" ]
        [ span
            [ className "todo-count" ]
            [ strong'
                [ text "0" ]
            , text " item left"
            ]
        , ul
            [ className "filters" ]
            [ li'
                [ a
                    [ className (if filter == All then "selected" else "")
                    , href "#/"
                    ]
                    [ text "All" ]
                ]
            , li'
                [ a
                    [ className (if filter == Active then "selected" else "")
                    , href "#/active"
                    ]
                    [ text "Active" ]
                ]
            , li'
                [ a
                    [ className (if filter == Completed then "selected" else "")
                    , href "#/completed"
                    ]
                    [ text "Completed" ]
                ]
            ]
        , button
             [ className "clear-completed" ]
             [ text "Clear Completed" ]
        ]

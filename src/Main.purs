module Main where

import Prelude hiding (div)

import Concur.React.Run (runWidgetInDom)
import Effect (Effect)
import Widgets.TodoApp (todoAppWidget)

main :: Effect Unit
main = runWidgetInDom "root" todoAppWidget
